﻿//#define QUADTREE_DEBUG

using Moonlit.Pathfinding.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Moonlit.Pathfinding
{
    public class QuadTree
    {
        internal static int maxLevel = 1;
        internal static int MaxLevel { get { return maxLevel; } }

        public static QuadNode root;

#if QUADTREE_DEBUG
        public static HashSet<QuadNode> nodes = new HashSet<QuadNode>();
#endif

        // DEBUG
#if QUADTREE_DEBUG
        public static HashSet<QuadNode> opened = new HashSet<QuadNode>();
#endif

        public QuadTree(Rect rect, int maxLevel)
        {
            QuadTree.maxLevel = maxLevel;
            root = new QuadNode(0, rect);
        }

        public QuadTree(Rect rect)
        {
            QuadTree.maxLevel = Mathf.RoundToInt(Mathf.Log(Mathf.Max(rect.width, rect.height), 2f));
            root = new QuadNode(0, rect);
        }

        public void Insert<T>(T value, Rect rect, bool force = false) where T : class
        {
            root.Insert(value, rect, force);
        }

        public void Insert<T>(T value, Vector2 point, bool force = false) where T : class
        {
            root.Insert(value, point, force);
        }

        public void DebugDraw(Color color)
        {
#if QUADTREE_DEBUG
            foreach (QuadNode n in nodes)
            {
                n.DebugDraw(color);
            }
#endif
        }

        public QuadNode GetNode(Vector2 point)
        {
            if (root.bounds.Contains(point))
            {
                return root.GetNode(point);
            }
            else
            {
                return null;
            }
        }

        public void Remove(QuadNode node)
        {
            node.Remove();
        }

        public void Remove(Rect area)
        {
            // Cannot iterate when removing - store nodes before deletion

            List<QuadNode> removeList = new List<QuadNode>();
            root.Retrieve(removeList, area);

            foreach (QuadNode qn in removeList)
            {
                qn.Remove();
            }
        }

        public void Clear()
        {
            root.Clear();
        }

        public IEnumerable<QuadNode> Retrieve(Rect area)
        {
            foreach (QuadNode qn in root.Retrieve(area))
            {
                yield return qn;
            }
        }
    }


    public class QuadNode : PathNode
    {
        [Flags]
        public enum Orientation : int
        {
            North = 1,
            NorthEast = 3,
            East = 2,
            SouthEast = 6,
            South = 4,
            SouthWest = 12,
            West = 8,
            NorthWest = 9
        }

        public int level;
        public Rect bounds;

        public Orientation orientation;

        public QuadNode container;

        public QuadNode northEast;
        public QuadNode southEast;
        public QuadNode southWest;
        public QuadNode northWest;

        public QuadNode north;
        public QuadNode east;
        public QuadNode south;
        public QuadNode west;

        public object holder = null;

        public QuadNode(int level, Rect bounds, QuadNode parent = null)
        {
            this.level = level;
            this.bounds = bounds;
            this.container = parent;

            // DEBUG
#if QUADTREE_DEBUG
            QuadTree.nodes.Add(this);
#endif
        }

        public void Clear()
        {
            if (holder != null)
            {
                //holder.Dispose();
                holder = null;
            }

            // DEBUG
            if (container != null)
            {
#if QUADTREE_DEBUG
                QuadTree.nodes.Remove(this);
#endif
            }

            if (northEast != null)
            {
                northEast.Clear();
                northEast = null;
            }

            if (southEast != null)
            {
                southEast.Clear();
                southEast = null;
            }

            if (southWest != null)
            {
                southWest.Clear();
                southWest = null;
            }

            if (northWest != null)
            {
                northWest.Clear();
                northWest = null;
            }
        }

        public void Remove()
        {
            // Make this node and all childs empty
            Clear();

            // If we have parent, and just made this node clear, check if upper node can be consolidated:
            if (container != null)
            {
                container.Consolidate();
            }
        }

        public void Consolidate()
        {
            // Check if was splitted
            if (northEast != null)
            {
                // If childs are empty
                if (northEast.IsEmpty() && southEast.IsEmpty() && southWest.IsEmpty() && northWest.IsEmpty())
                {
#if QUADTREE_DEBUG
                    // DEBUG
                    QuadTree.nodes.Remove(northEast);
                    QuadTree.nodes.Remove(southEast);
                    QuadTree.nodes.Remove(southWest);
                    QuadTree.nodes.Remove(northWest);
#endif

                    // Make this node empty
                    northEast = null;
                    southEast = null;
                    southWest = null;
                    northWest = null;

                    // Check if we can go up to consolidate
                    if (container != null)
                    {
                        container.Consolidate();
                    }
                }
            }
        }

        public bool IsEmpty()
        {
            return northEast == null && holder == null;
        }

        public void Split()
        {
            // TODO: Optimize this:
            float x = bounds.xMin;
            float y = bounds.yMin;
            float subWidth = bounds.width / 2f;
            float subHeight = bounds.height / 2f;

            int subLevel = level + 1;


            northEast = new QuadNode(subLevel, new Rect(x + subWidth, y + subHeight, subWidth, subHeight), this);
            northEast.orientation = Orientation.NorthEast;

            southEast = new QuadNode(subLevel, new Rect(x + subWidth, y, subWidth, subHeight), this);
            southEast.orientation = Orientation.SouthEast;

            southWest = new QuadNode(subLevel, new Rect(x, y, subWidth, subHeight), this);
            southWest.orientation = Orientation.SouthWest;

            northWest = new QuadNode(subLevel, new Rect(x, y + subHeight, subWidth, subHeight), this);
            northWest.orientation = Orientation.NorthWest;

            northEast.south = southEast;
            northEast.west = northWest;

            southEast.north = northEast;
            southEast.west = southWest;

            southWest.north = northWest;
            southWest.east = southEast;

            northWest.east = northEast;
            northWest.south = southWest;

            if (container != null)
            {
                switch (orientation)
                {
                    case Orientation.NorthEast:
                        northEast.north = container.north;
                        northEast.east = container.east;

                        southEast.east = container.east;
                        southEast.south = container.southEast;

                        southWest.south = container.southEast;
                        southWest.west = container.northWest;

                        northWest.north = container.north;
                        northWest.west = container.northWest;
                        break;
                    case Orientation.SouthEast:
                        northEast.north = container.northEast;
                        northEast.east = container.east;

                        southEast.east = container.east;
                        southEast.south = container.south;

                        southWest.south = container.south;
                        southWest.west = container.southWest;

                        northWest.north = container.northEast;
                        northWest.west = container.southWest;
                        break;
                    case Orientation.SouthWest:
                        northEast.north = container.northWest;
                        northEast.east = container.southEast;

                        southEast.east = container.southEast;
                        southEast.south = container.south;

                        southWest.south = container.south;
                        southWest.west = container.west;

                        northWest.north = container.northWest;
                        northWest.west = container.west;
                        break;
                    case Orientation.NorthWest:
                        northEast.north = container.north;
                        northEast.east = container.northEast;

                        southEast.east = container.northEast;
                        southEast.south = container.southWest;

                        southWest.south = container.southWest;
                        southWest.west = container.west;

                        northWest.north = container.north;
                        northWest.west = container.west;
                        break;
                }
            }
        }

        public void Insert<T>(T value, Rect rect, bool force = false) where T : class
        {
            // Check which nodes are being overlapped by rect:
            if (level < QuadTree.maxLevel)
            {
                if (northEast == null)
                {
                    Split();
                }

                if (rect.Overlaps(northEast.bounds))
                {
                    northEast.Insert(value, rect, force);
                }

                if (rect.Overlaps(southEast.bounds))
                {
                    southEast.Insert(value, rect, force);
                }

                if (rect.Overlaps(southWest.bounds))
                {
                    southWest.Insert(value, rect, force);
                }

                if (rect.Overlaps(northWest.bounds))
                {
                    northWest.Insert(value, rect, force);
                }
            }
            else
            {
                if (holder == null || force) holder = value;
            }
        }

        public void Insert<T>(T value, Vector2 point, bool force = false) where T : class
        {
            // Check which nodes are being overlapped by rect:
            if (level < QuadTree.maxLevel)
            {
                if (northEast == null)
                {
                    Split();
                }

                if (northEast.bounds.Contains(point))
                {
                    northEast.Insert(value, point, force);
                }

                else if (southEast.bounds.Contains(point))
                {
                    southEast.Insert(value, point, force);
                }

                else if (southWest.bounds.Contains(point))
                {
                    southWest.Insert(value, point, force);
                }

                else if (northWest.bounds.Contains(point))
                {
                    northWest.Insert(value, point, force);
                }
            }
            else
            {
                if (holder == null || force) holder = value;
            }
        }

        public void GetNeighbours(List<QuadNode> list)
        {
            Rect testBounds = new Rect(bounds);
            testBounds.width += 0.1f;
            testBounds.height += 0.1f;
            testBounds.center = bounds.center;

            QuadTree.root.Retrieve(list, testBounds);
        }

        public IEnumerable<QuadNode> GetNeighbours()
        {
            Rect horizontal = new Rect(bounds);
            horizontal.width += 0.1f;
            horizontal.center = bounds.center;

            Rect vertical = new Rect(bounds);
            vertical.height += 0.1f;
            vertical.center = bounds.center;

            if (north != null) foreach (QuadNode qn in north.Neighbours(horizontal, vertical)) yield return qn;
            if (east != null) foreach (QuadNode qn in east.Neighbours(horizontal, vertical)) yield return qn;
            if (south != null) foreach (QuadNode qn in south.Neighbours(horizontal, vertical)) yield return qn;
            if (west != null) foreach (QuadNode qn in west.Neighbours(horizontal, vertical)) yield return qn;
        }

        public void Retrieve(List<QuadNode> list, Rect rect)
        {
            if (rect.Overlaps(bounds))
            {
                if (northEast == null)
                {
                    list.Add(this);
                }
                else
                {
                    northEast.Retrieve(list, rect);
                    southEast.Retrieve(list, rect);
                    southWest.Retrieve(list, rect);
                    northWest.Retrieve(list, rect);
                }
            }
        }

        public IEnumerable<QuadNode> Retrieve(Rect rect)
        {
            if (bounds.Overlaps(rect))
            {
                if (northEast == null)
                {
                    yield return this;
                }
                else
                {
                    foreach (QuadNode n in northEast.Retrieve(rect))
                    {
                        yield return n;
                    }
                    foreach (QuadNode n in southEast.Retrieve(rect))
                    {
                        yield return n;
                    }
                    foreach (QuadNode n in southWest.Retrieve(rect))
                    {
                        yield return n;
                    }
                    foreach (QuadNode n in northWest.Retrieve(rect))
                    {
                        yield return n;
                    }
                }
            }
        }

        private IEnumerable<QuadNode> Neighbours(Rect horizontal, Rect vertical)
        {
            if (bounds.Overlaps(horizontal) || bounds.Overlaps(vertical))
            {
                if (northEast == null)
                {
                    yield return this;
                }
                else
                {
                    foreach (QuadNode n in northEast.Neighbours(horizontal, vertical))
                    {
                        yield return n;
                    }
                    foreach (QuadNode n in southEast.Neighbours(horizontal, vertical))
                    {
                        yield return n;
                    }
                    foreach (QuadNode n in southWest.Neighbours(horizontal, vertical))
                    {
                        yield return n;
                    }
                    foreach (QuadNode n in northWest.Neighbours(horizontal, vertical))
                    {
                        yield return n;
                    }
                }
            }
        }

        public QuadNode GetNode(Vector2 point)
        {
            if (northEast != null)
            {
                if (northEast.bounds.Contains(point))
                {
                    return northEast.GetNode(point);
                }
                else if (southEast.bounds.Contains(point))
                {
                    return southEast.GetNode(point);
                }
                else if (southWest.bounds.Contains(point))
                {
                    return southWest.GetNode(point);
                }
                else if (northWest.bounds.Contains(point))
                {
                    return northWest.GetNode(point);
                }
                else
                {
                    return null;
                }
            }
            return this;
        }

        public void DebugDraw(Color color, int z = 0)
        {
            bounds.DebugDraw(color, z);
        }

        public override IEnumerable<PathNode> Neighbours()
        {
            foreach (PathNode pn in GetNeighbours())
            {
#if QUADTREE_DEBUG
                QuadTree.opened.Add((QuadNode)pn);
#endif
                yield return pn;
            }
        }
        //public override bool IsBlocked(PathNode from, IPathAgent agent)
        //{
        //    return holder != null;
        //}
    }
}
