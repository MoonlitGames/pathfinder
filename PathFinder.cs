﻿#define PATHFINDER_REPARENT

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Moonlit.Pathfinding
{
    using Moonlit.Pathfinding.Utils;

    public static class PathFinder
    {
        // DEBUG: Opened nodes counter:
        public static int VisitedNodes = 0;

        // Current search index, used to invalidate previously used nodes
        internal static int _searchIndex = int.MinValue;

        //BinaryHeap<PathNode> Opened = new BinaryHeap<PathNode>();
        //SortedDictionary<int, PathNode> Opened = new SortedDictionary<int, PathNode>();
        //static List<PathNode> Opened = new List<PathNode>();
        //static MinHeap<int, PathNode> Opened = new MinHeap<int, PathNode>(230400);
        //static PathNodeQueue Opened = new HeapQueue(230400);
        static readonly PathNodeQueue _opened = new PresortedList();

        public static PathNode AStarSearch<N, A>(N start, N goal, A agent, PathNodeQueue pnq = null)
            where N : PathNode
            where A : IPathAgent
        {
            // Increment search index to invalidate all previous nodes;
            ++_searchIndex;

            // Clearing Opened collections for further use:
            _opened.Clear(); // Most of the time not empty after previous uses

            // Check if start tile has Node attached, if no create one, otherwise reset the old one:
            PathNode current = start;
            start.Reset(null);

            // Total movement cost is 0
            current.costFromStart = 0;

            // Calculate heuristic for start node:
            current.costToGoal = agent.CostToGoal(start, goal);

            // Precompute total cost:
            current.UpdateTotalCost();

            // In case of not existing path, store closest node:
            PathNode closest = current;

            //// Insert startNode to opened list:
            //Opened.Enqueue(startNode);

            // DEBUG: Opened nodes counter:
            VisitedNodes = 0;

            // Loop over Opened nodes:
            while (true)
            {
                // Close current node to prevent unnecessary checks:
                current.closed = true;

                // If we found node that is our goal, return the path reversed, with success info:
                if (current.Equals(goal))
                {
                    // TODO: Return path from start to goal, store error/info message about search status.
                    return current;
                }

                // Check and store closest node in case of not existing path:
                if (current.totalCost < closest.totalCost) closest = current;

                // Loop over adjacent / neighbouring nodes to our current tile:
                foreach (PathNode neighbour in current.Neighbours())
                {
                    // If path is blocked, skip this tile:
                    if (neighbour == null || agent.IsBlocked(current, neighbour)) continue;

                    // Check if index of neighbour node is up-to-date with current search - search index must be equal to currently used:
                    // If index is the same as global index, node must've been opened in this session:
                    else if (neighbour.IsValid()) // <- this means that node was Opened in current session!
                    {
                        // HERE we deal with valid, existing nodes - ones that are at least in opened list, and might me in closed (marked by flag):

                        // If neighbour node is closed skip it:
                        if (neighbour.closed) continue;

#if PATHFINDER_REPARENT
                        // If we found that it is better to acces neighbourNode from currentNode than from its original parent, re-parent it:
                        int cost = current.costFromStart + agent.TraverseCost(current, neighbour);

                        if (neighbour.costFromStart >= cost)
                        {
                            neighbour.costFromStart = cost;
                            neighbour.parent = current;
                            neighbour.UpdateTotalCost();

                            // Remove and reinsert node to sorted list:
                            _opened.Update(neighbour);
                        }
#endif


                        // DEBUG: Increment number of visited nodes
                        ++VisitedNodes;

                        // Move to next neighbour:
                        continue;
                    }
                    else
                    {
                        // Otherwise, neighbourNode is invalid and must be resetted
                        neighbour.Reset(current);

                        // Cost from start node to this neighbour = cost to previous node + traverse cost to this node:
                        neighbour.costFromStart = current.costFromStart + agent.TraverseCost(current, neighbour);

                        // Cost to goal is calculate with user-defined heuristic:
                        neighbour.costToGoal = agent.CostToGoal(neighbour, goal);

                        // Precompute total cost:
                        neighbour.UpdateTotalCost();

                        // Add resetted node to opened list;
                        _opened.Enqueue(neighbour);

                        // DEBUG: Increment number of visited nodes
                        ++VisitedNodes;

                        // Move to next neighbour:
                        continue;
                    }
                } // End loop over neighbouring nodes

                // If there are nodes left, dequeue and continue:
                if (_opened.Count > 0)
                    current = _opened.Dequeue();
                else break; // No open nodes left - break and return closest

            } // End loop over Opened nodes

            // Return closes path, if no connection was found:
            return closest;

        } // End AStarSearch
    } // End PathFinder

    public abstract class PathNode : IComparable<PathNode>
    {
        // Parent node that leads to this node
        public PathNode parent;

        // Stores the index of last AStarSearch
        internal int searchIndex;

        // G(x) - move cost from start node, calculated by agent
        internal int costFromStart;

        // H(x) - heuristic cost to goal, calculated by agent
        internal int costToGoal;

        // F(x) = G(x) + H(x)
        internal int totalCost;

        //// Default constructor for PathNode
        //internal PathNode(PathNode parent)
        //{
        //    this.parent = parent;
        //    searchIndex = PathFinder._searchIndex;
        //}

        // This method is used to make PathNode valid - update it status to match current search
        // Note that not all pools are updated!
        internal void Reset(PathNode parent)
        {
            this.parent = parent;
            closed = false;
            searchIndex = PathFinder._searchIndex;
        }

        // Pre-compute F(x), and store it
        internal void UpdateTotalCost()
        {
            totalCost = costFromStart + costToGoal;
        }

        // Check if PathNode is up-to-date with current search - if it's valid or not
        internal bool IsValid()
        {
            return PathFinder._searchIndex == searchIndex;
        }

        // Flag determining wheter we are done with checking this node (reduces need for closed list)
        internal bool closed = false;

        // Compare to other node - used to sort
        public int CompareTo(PathNode other)
        {
            return other.totalCost > totalCost ? -1 : 1;
        }

        public IEnumerable<PathNode> GetPrevious()
        {
            PathNode current = this;
            while (current != null)
            {
                yield return current;
                current = current.parent;
            }
        }

        public void GetPath(List<PathNode> path)
        {
            PathNode current = this;

            while (current != null)
            {
                path.Insert(0, current);
                current = current.parent;
            }
        }

        #region Abstract methods for end-user

        /// <summary>
        /// Number of nodes in neighbourhood. Works strictly with GetNeighbour(n).
        /// </summary>
        /// <returns>Number of this node's neighbours</returns>
        //public abstract int NeighboursCount();

        /// <summary>
        /// Return neighbour at specified index. Index must not exceed number-1 returned by NeighboursCount().
        /// </summary>
        /// <param name="n">This node's neighbour with number n. N is counted from 0.</param>
        /// <returns>Neighbouring node.</returns>
        public abstract IEnumerable<PathNode> Neighbours();

        /// <summary>
        /// Check whether path for this Agent from specified Node is blocked.
        /// </summary>
        /// <param name="from">Node from which Agent is coming.</param>
        /// <param name="agent">Agent that is traversing.</param>
        /// <returns>True if Agent cannot pass, false otherwise.</returns>
        //public abstract bool IsBlocked(PathNode from, IPathAgent agent);

        #endregion
    }

    public interface IPathAgent
    {
        int CostToGoal(PathNode current, PathNode goal);
        int TraverseCost(PathNode start, PathNode current);
        bool IsBlocked(PathNode from, PathNode to);
    }

    #region Test Collections for fastest Opened priority Queue

    internal static class ExtensionMethods
    {
        internal static void InsertSorted(this List<PathNode> list, PathNode node)
        {
            int index = list.BinarySearch(node);
            if (index < 0) index = ~index;
            list.Insert(index, node);
        }

        internal static PathNode RemoveFirst(this List<PathNode> list)
        {
            PathNode node = list[0];
            list.Remove(node);
            return node;
        }

        internal static void Update(this List<PathNode> list, PathNode node)
        {
            list.Remove(node);
            list.InsertSorted(node);
        }

        internal static void InsertSorted(this SortedDictionary<int, PathNode> dict, PathNode node)
        {
            dict.Add(node.totalCost, node);
        }
    }

    public interface PathNodeQueue
    {
        void Enqueue(PathNode node);

        PathNode Dequeue();

        void Update(PathNode node);

        void Clear();

        int Count { get; }
    }

    internal class PresortedList : List<PathNode>, PathNodeQueue
    {
        /// <summary>
        /// O(n) + O(logn)
        /// </summary>
        public void Enqueue(PathNode node)
        {
            int index = this.BinarySearch(node);
            if (index < 0) index = ~index;
            this.Insert(index, node);
        }

        /// <summary>
        /// O(n)
        /// </summary>
        public PathNode Dequeue()
        {
            PathNode node = this[0];
            RemoveAt(0);
            return node;
        }

        /// <summary>
        /// O(n) + O(n) + O(logn)
        /// </summary>
        public void Update(PathNode node)
        {
            Remove(node);
            Enqueue(node);
        }
    }

    #endregion
}