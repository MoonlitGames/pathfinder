﻿//#define TUNNEL_DEBUG

using Moonlit.Pathfinding.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Moonlit.Pathfinding
{
    public class TunnelSmooth
    {
        [Flags]
        public enum Direction : int
        {
            None = 0,
            Horizontal = 2,
            Rising = 4,
            Up = 12,
            Right = 22,
            Down = 32,
            Left = 66
        }

        public class Tunnel
        {
            // DEBUG
#if TUNNEL_DEBUG
            public static HashSet<Tunnel> tunnels = new HashSet<Tunnel>();
#endif

            // DEBUG
            public static void DebugDraw(int z)
            {
#if TUNNEL_DEBUG
                foreach (Tunnel t in tunnels)
                {
                    t.rect.DebugDraw(Color.magenta, z);
                }
#endif
            }

            public QuadNode startNode;
            public QuadNode endNode;
            public Rect rect;
            public Direction direction;

            public Tunnel(QuadNode node)
            {
#if TUNNEL_DEBUG
                tunnels.Add(this);
#endif
                startNode = node;
                rect = new Rect(node.bounds);
            }

            public bool Expand(QuadNode next)
            {
                // Assign new direction on first expansion
                if (direction == Direction.None)
                {
                    direction = GetDirection(rect, next.bounds);
                }
                // If expansion direction is different than current
                else if (direction != GetDirection(rect, next.bounds))
                {
                    return false;
                }

                switch (direction)
                {
                    case Direction.Up:
                        if (!rect.ContainsHorizontally(next.bounds)) return false;

                        rect.yMax = next.bounds.yMax;
                        ShrinkWidth(ref rect, next.bounds);
                        break;

                    case Direction.Down:
                        if (!rect.ContainsHorizontally(next.bounds)) return false;

                        rect.yMin = next.bounds.yMin;
                        ShrinkWidth(ref rect, next.bounds);
                        break;

                    case Direction.Left:
                        if (!rect.ContainsVertically(next.bounds)) return false;

                        rect.xMin = next.bounds.xMin;
                        ShrinkHeight(ref rect, next.bounds);
                        break;

                    case Direction.Right:
                        if (!rect.ContainsVertically(next.bounds)) return false;

                        rect.xMax = next.bounds.xMax;
                        ShrinkHeight(ref rect, next.bounds);
                        break;

                    // Cannot expand on other directions!
                    default:
                    case Direction.None:
                        return false;
                }

                return true;
            }

            public Direction GetDirection(Rect from, Rect to)
            {
                // Horizontal
                if (Mathf.Abs(to.center.x - from.center.x) > Mathf.Abs(to.center.y - from.center.y))
                {
                    return to.center.x > from.center.x ? Direction.Right : Direction.Left;
                }
                // Vertical
                else
                {
                    return to.center.y > from.center.y ? Direction.Up : Direction.Down;
                }
            }

            private static void ShrinkWidth(ref Rect rect, Rect other)
            {
                rect.xMin = other.xMin > rect.xMin ? other.xMin : rect.xMin;
                rect.xMax = other.xMax < rect.xMax ? other.xMax : rect.xMax;
            }

            private static void ShrinkHeight(ref Rect rect, Rect other)
            {
                rect.yMin = other.yMin > rect.yMin ? other.yMin : rect.yMin;
                rect.yMax = other.yMax < rect.yMax ? other.yMax : rect.yMax;
            }

            public float MiddleWidth()
            {
                return (rect.xMin + rect.xMax) / 2f;
            }

            public float MiddleHeight()
            {
                return rect.center.y;
            }

            public Vector2 Resolve(Vector2 point)
            {
                if ((direction & Direction.Horizontal) > 0)
                {
                    return new Vector2(point.x, MiddleHeight());
                }
                else
                {
                    return new Vector2(MiddleWidth(), point.y);
                }
            }

            internal void Connect(Tunnel nextTunnel, List<Vector2> path)
            {
                // Tunnels are parallel, if flag check is bigger than 0 (bit are left)
                if (((direction ^ nextTunnel.direction) & Direction.Horizontal) == 0)
                {
#if TUNNEL_DEBUG
                    Debug.Log("Resolving parallel:");
#endif
                    // Both are horizontal
                    if ((direction & Direction.Horizontal) > 0)
                    {
                        //float width = rect.xMax > nextTunnel.rect.xMax ? (nextTunnel.rect.xMax + rect.xMin) / 2f : (rect.xMax + nextTunnel.rect.xMin);
                        //float width = (Mathf.Max(rect.xMin, nextTunnel.rect.xMin) + Mathf.Min(rect.xMax, nextTunnel.rect.xMax)) * 0.5f;
                        //float width = Mathf.Clamp(nextTunnel.startNode.bounds.center.x, endNode.bounds.xMin * 0.5f, endNode.bounds.xMax * 0.5f);
                        float width = endNode.bounds.width >= nextTunnel.startNode.bounds.width ? endNode.bounds.centerX() : nextTunnel.startNode.bounds.centerX();
#if TUNNEL_DEBUG
                        Debug.Log(" - horizontal to horizontal");
#endif
                        path.Add(new Vector2(width, MiddleHeight()));
                        path.Add(new Vector2(width, nextTunnel.MiddleHeight()));
                    }
                    // Both are vertical
                    else
                    {
                        //float height = rect.yMax > nextTunnel.rect.yMax ? (nextTunnel.rect.yMax + rect.yMin) / 2f : (rect.yMax + nextTunnel.rect.yMin);
                        //float height = (Mathf.Max(rect.yMin, nextTunnel.rect.yMin) + Mathf.Min(rect.yMax, nextTunnel.rect.yMax)) * 0.5f;
                        //float height = Mathf.Clamp(nextTunnel.startNode.bounds.center.y, endNode.bounds.yMin * 0.5f, endNode.bounds.yMax * 0.5f);
                        float height = endNode.bounds.height >= nextTunnel.startNode.bounds.height ? endNode.bounds.centerY() : nextTunnel.startNode.bounds.centerY();
#if TUNNEL_DEBUG
                        Debug.Log(" - vertical to vertical");
#endif

                        path.Add(new Vector2(MiddleWidth(), height));
                        path.Add(new Vector2(nextTunnel.MiddleWidth(), height));
                    }
                }
                // Tunnels are perpendicular
                else
                {
#if TUNNEL_DEBUG
                    Debug.Log("Resolving perpendicular:");
#endif

                    // Current is horizontal
                    if ((direction & Direction.Horizontal) > 0)
                    {
#if TUNNEL_DEBUG
                        Debug.Log(" - horizontal to vertical");
#endif

                        if (endNode.bounds.width <= nextTunnel.startNode.bounds.width)
                        {
                            path.Add(new Vector2(endNode.bounds.centerX(), MiddleHeight()));
                            path.Add(new Vector2(endNode.bounds.centerX(), nextTunnel.startNode.bounds.centerY()));
                            path.Add(new Vector2(nextTunnel.MiddleWidth(), nextTunnel.startNode.bounds.centerY()));
                        }
                        else
                        {
                            path.Add(new Vector2(nextTunnel.MiddleWidth(), MiddleHeight()));
                        }

                        //path.Add(new Vector2(nextTunnel.MiddleWidth(), MiddleHeight()));
                    }
                    else
                    {
#if TUNNEL_DEBUG
                        Debug.Log(" - vertical to horizontal");
#endif

                        if (endNode.bounds.width <= nextTunnel.startNode.bounds.width)
                        {
                            path.Add(new Vector2(MiddleWidth(), endNode.bounds.centerY()));
                            path.Add(new Vector2(nextTunnel.startNode.bounds.centerX(), endNode.bounds.centerY()));
                            path.Add(new Vector2(nextTunnel.startNode.bounds.centerX(), nextTunnel.MiddleHeight()));
                        }
                        else
                        {
                            path.Add(new Vector2(MiddleWidth(), nextTunnel.MiddleHeight()));
                        }
                        //path.Add(new Vector2(MiddleWidth(), nextTunnel.MiddleHeight()));
                    }
                }
            }
        }

        private static Vector2 ResolveEndPoint(Vector2 end, QuadNode endNode)
        {
            if (!endNode.bounds.Contains(end))
            {

                end.x = Mathf.Clamp(end.x, endNode.bounds.xMin, endNode.bounds.xMax);
                end.y = Mathf.Clamp(end.y, endNode.bounds.yMin, endNode.bounds.yMax);
            }

            return end;
        }

        public static List<Vector2> ProcessPath(Vector2 start, Vector2 end, QuadNode endNode)
        {
#if TUNNEL_DEBUG
            Tunnel.tunnels.Clear();
#endif

            if (endNode == null) return null;

            List<Vector2> path = new List<Vector2>();

            // Resolve in single node
            if (endNode.parent == null)
            {
                path.Add(start);
                path.Add(new Vector2(start.x, end.y));
                path.Add(ResolveEndPoint(end, endNode));

                return path;
            }

            // Process path over multiple nodes

            // Add start position
            path.Add(ResolveEndPoint(end, endNode));

            QuadNode previous = null;
            QuadNode current = endNode;

            Tunnel previousTunnel = null;
            Tunnel currentTunnel = null;

            while (true)
            {
                if (currentTunnel == null)
                {
#if TUNNEL_DEBUG
                    Debug.Log("Creating new tunnel in main loop.");
#endif

                    currentTunnel = new Tunnel(current);
                }
                else
                {
                    // Try to expand current tunnel, if fails do as follows:
                    if (!currentTunnel.Expand(current))
                    {
#if TUNNEL_DEBUG
                        Debug.Log("Cannot expand! Resolving");
#endif

                        // Compute previous tunnel end / current tunnel start points
                        currentTunnel.endNode = previous;

                        if (previousTunnel == null)
                        {
#if TUNNEL_DEBUG
                            Debug.Log("PreviousTunnel = null, resolving point.");
#endif

                            // Add point between start and current tunnel start
                            path.Add(currentTunnel.Resolve(ResolveEndPoint(end, endNode)));
                        }
                        else
                        {
#if TUNNEL_DEBUG
                            Debug.Log("Resolving Tunnel connection.");
#endif
                            previousTunnel.Connect(currentTunnel, path);
                        }

                        previousTunnel = currentTunnel;
                        currentTunnel = new Tunnel(current);
                    }
                }

                if (current.parent == null)
                {
#if TUNNEL_DEBUG
                    Debug.Log("No more nodes, resolving whats left");
#endif

                    if (previousTunnel == null)
                    {
#if TUNNEL_DEBUG
                        Debug.Log("Resolving start");
#endif

                        path.Add(currentTunnel.Resolve(ResolveEndPoint(end, endNode)));
                        path.Add(currentTunnel.Resolve(start));
                    }
                    else
                    {
#if TUNNEL_DEBUG
                        Debug.Log("Resolving end");
#endif

                        previousTunnel.Connect(currentTunnel, path);
                        path.Add(currentTunnel.Resolve(start));
                    }
                    break;
                }

                previous = current;
                current = (QuadNode)current.parent;
            }

            path.Add(start);

#if TUNNEL_DEBUG
            foreach (Vector2 pos in path)
            {
                Debug.Log(pos);
            }
#endif
            return path;
        }
    }

    internal static class RectExtensionMethods
    {
        public static bool ContainsVertically(this Rect rect, Rect other)
        {
            return (other.yMin >= rect.yMin && other.yMax <= rect.yMax) || (rect.yMin >= other.yMin && rect.yMax <= other.yMax);
        }

        public static bool ContainsHorizontally(this Rect rect, Rect other)
        {
            return (other.xMin >= rect.xMin && other.xMax <= rect.xMax) || (rect.xMin >= other.xMin && rect.xMax <= other.xMax);
        }

        public static float centerX(this Rect rect)
        {
            return (rect.xMin + rect.xMax) * 0.5f;
        }

        public static float centerY(this Rect rect)
        {
            return (rect.yMin + rect.yMax) * 0.5f;
        }
    }
}