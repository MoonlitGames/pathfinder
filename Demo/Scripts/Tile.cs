﻿using System;
using UnityEngine;

namespace Moonlit.Pathfinding.Demo
{
    public class Tile : MonoBehaviour, IDisposable
    {
        public void Dispose()
        {
            // TODO
        }

        void Start()
        {
            Rect rect = new Rect();
            rect.size = Vector2.one;
            rect.center = transform.position;
            Demo.quadTree.Insert(this, rect);
        }
    }
}