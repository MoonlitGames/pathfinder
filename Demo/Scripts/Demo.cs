﻿using Moonlit.Pathfinding.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Moonlit.Pathfinding.Demo
{
    /// <summary>
    /// No Grid - only snapping to grid
    /// </summary>
    [RequireComponent(typeof(MeshRenderer))]
    public class Demo : MonoBehaviour
    {
        [Space(20)]
        [Header("Settings")]
        public int width = 128;
        public int height = 128;
        public float tileSize = 1;
        public GameObject tilePrefab;
        public bool debugEnabled = true;

        [Space(20)]
        [Header("Mouse")]
        public Vector2 mouseScreenPosition;
        public Vector2 mouseWorldPosition;
        public Vector2 mouseGridPosition;
        public Vector2 worldFromGridPosition;
        public int mouseNeighbours;
        public QuadNode mouseNode;

        [Space(20)]
        [Header("Info")]
        public int tileCount;

        [Space(20)]
        // Private
        PathNode closestNode;
        List<Vector2> path;
        private Vector2 pathStartPosition;
        private Vector2 pathEndPosition;

        private bool drawOverlaps;
        private bool drawOverlapsFast;
        private List<QuadNode> mouseNeighbourhood = new List<QuadNode>();
        private Vector2 previousMouseWorldPosition;
        private GameObject tileHolder;
        private bool pathReady;

        public static QuadTree quadTree;

        void Start()
        {
            transform.localScale = new Vector3(width * tileSize, height * tileSize, 1f);
            ResizeAndTile();

            tileHolder = new GameObject("Tiles") as GameObject;

            Rect rect = new Rect();
            rect.width = width;
            rect.height = height;
            rect.center = Vector2.zero;

            quadTree = new QuadTree(rect);
        }

        void ResizeAndTile()
        {
            Vector2 offset = new Vector2(
                x: (-((width) / 2f)),
                y: (-((height) / 2f))
                );

            renderer.material.mainTextureScale = transform.localScale / tileSize;
            renderer.material.mainTextureOffset = offset;
        }

        // Update is called once per frame
        void Update()
        {
            // Update mouse positions, etc.
            mouseScreenPosition = Input.mousePosition;
            mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseScreenPosition);
            mouseGridPosition = WorldToGridPosition(mouseWorldPosition);
            worldFromGridPosition = GridToWorldPosition(mouseGridPosition);

            if (QuadTree.root.bounds.Contains(mouseWorldPosition))
            {
                mouseNode = quadTree.GetNode(mouseWorldPosition);
            }

            drawOverlaps = false;
            drawOverlapsFast = false;

            // Handle user input
            // If not moving screen:
            if (!Input.GetKey(KeyCode.Space) && !Input.GetMouseButton(2))
            {
                if (Input.GetKey(KeyCode.LeftAlt))
                {
                    drawOverlaps = true;
                }
                else if (Input.GetKey(KeyCode.Z))
                {
                    drawOverlapsFast = true;
                }
                else if (Input.GetKey(KeyCode.LeftControl))
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        pathStartPosition = mouseWorldPosition;
                    }
                    else if (Input.GetMouseButtonUp(1))
                    {
                        pathEndPosition = mouseWorldPosition;

                        FindPath();
                    }
                }
                else if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
                {
                    int toPlace = Mathf.FloorToInt(Vector2.Distance(previousMouseWorldPosition, mouseWorldPosition) / tileSize);

                    if (toPlace >= 0)
                    {
                        Vector2 step = (mouseWorldPosition - previousMouseWorldPosition).normalized * tileSize;

                        for (int i = 0; i <= toPlace; ++i)
                        {
                            Vector3 pos = i * step + previousMouseWorldPosition;
                            if (QuadTree.root.bounds.Contains(pos))
                            {
                                PlaceTile(pos, Input.GetMouseButton(1));
                            }
                        }
                        if (pathReady)
                        {
                            FindPath();
                        }
                    }
                }
                else if (Input.GetKeyDown(KeyCode.C))
                {
                    quadTree.Clear();
                    tileCount = 0;
                }
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                debugEnabled = !debugEnabled;
            }

            OnDebugDraw();

            previousMouseWorldPosition = mouseWorldPosition;
        }

        void FindPath()
        {
            QuadNode startNode = quadTree.GetNode(pathStartPosition);
            QuadNode endNode = quadTree.GetNode(pathEndPosition);

#if QUADTREE_DEBUG
            QuadTree.opened.Clear();
#endif

            long startTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            closestNode = PathFinder.AStarSearch(startNode, endNode, QuadTreeTraversor.Instance);

            path = TunnelSmooth.ProcessPath(pathStartPosition, pathEndPosition, (QuadNode)closestNode);

            long endTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            Debug.Log("Path " + (closestNode == null ? "not" : "") + " found. Time: " + (endTime - startTime) + " ms / Visited Nodes: " + PathFinder.VisitedNodes);

            pathReady = true;
        }

        void DrawPath(int z)
        {
            if (!pathReady) return;

            //PathNode node = closestNode;
            Vector3 offset = Vector3.forward * z;

#if QUADTREE_DEBUG
            if (debugEnabled)
            {
                Color orange = new Color(0.7f, 0.5f, 0f);

                foreach (QuadNode qn in QuadTree.opened)
                {
                    qn.bounds.DebugDraw(Color.yellow, z);
                }
            }
#endif

            //List<Vector2> positions = TunnelSmooth.ProcessPath(pathStartPosition, pathEndPosition, (QuadNode)node);
            //StraightenPath((QuadNode)path, positions, z);
            Vector3 prevPos = path[0];
            int i = 0;

            foreach (Vector2 pos in path)
            {
                Debug.DrawLine((Vector3)prevPos + offset, (Vector3)pos + offset, ++i % 2 == 0 ? Color.green : Color.black);
                prevPos = pos;
            }

            TunnelSmooth.Tunnel.DebugDraw(z);

            //while (node != null)
            //{
            //    QuadNode qn = (QuadNode)node;

            //    if (debugEnabled)
            //    {
            //        qn.DebugDraw(Color.green, z);
            //    }

            //    if (node.parent != null)
            //    {
            //        QuadNode qnParent = (QuadNode)node.parent;
            //        Debug.DrawLine((Vector3)qnParent.bounds.center + offset, (Vector3)qn.bounds.center + offset, Color.green);
            //        node = node.parent;
            //    }
            //    else
            //    {
            //        break;
            //    }
            //}
        }

        List<Vector3> GetPathPositions(QuadNode lastNode, int z)
        {
            QuadNode current = lastNode;

            if (current == null || current.parent == null) return null;

            Vector3 offset = Vector3.forward * z;

            QuadNode next = (QuadNode)current.parent;

            List<Vector3> positions = new List<Vector3>();

            positions.Add((Vector3)current.bounds.center + offset);

            while (next != null)
            {
                if (debugEnabled)
                {
                    current.DebugDraw(Color.green, z);
                }

                if (Mathf.Abs(current.bounds.center.x - next.bounds.center.x) > Mathf.Abs(current.bounds.center.y - next.bounds.center.y))
                {
                    // HORIZONTAL MOVEMENT

                    // From bigger to smaller
                    if (current.bounds.width > next.bounds.width)
                    {
                        positions.Add(new Vector3(current.bounds.center.x, next.bounds.center.y, z));
                    }
                    // From smaller to bigger or equal
                    else
                    {
                        positions.Add(new Vector3(next.bounds.center.x, current.bounds.center.y, z));
                    }

                    //positions.Add(new Vector3(next.bounds.center.x, current.bounds.center.y, z));
                }
                else
                {
                    // VERTICAL MOVEMENT

                    // From bigger to smaller
                    if (current.bounds.width > next.bounds.width)
                    {
                        positions.Add(new Vector3(next.bounds.center.x, current.bounds.center.y, z));
                    }
                    // From smaller to bigger or equal
                    else
                    {
                        positions.Add(new Vector3(current.bounds.center.x, next.bounds.center.y, z));
                    }


                    positions.Add(new Vector3(current.bounds.center.x, next.bounds.center.y, z));

                }

                current = next;
                next = (QuadNode)next.parent;
            }

            return positions;
        }

        void StraightenPath(QuadNode lastNode, List<Vector3> list, int z)
        {
            if (lastNode == null) return;

            if (lastNode.parent == null)
            {
                // Resolve in one node
                return;
            }

            QuadNode previous = lastNode;
            QuadNode current = (QuadNode)lastNode.parent;
            Vector3 start = previous.bounds.center;
            //Vector3 stop = current.bounds.center;

            bool vertical = Mathf.Abs(current.bounds.center.x - previous.bounds.center.x) < Mathf.Abs(current.bounds.center.y - previous.bounds.center.y);

            Window window;

            if (vertical)
            {
                window = new Window(previous.bounds.xMin, previous.bounds.xMax);
            }
            else
            {
                window = new Window(previous.bounds.yMin, previous.bounds.yMax);
            }


            while (current != null)
            {
                // Measure distance on each axis - if on X is bigger, than we're moving horizontally
                if ((Mathf.Abs(current.bounds.center.x - previous.bounds.center.x) < Mathf.Abs(current.bounds.center.y - previous.bounds.center.y)) != vertical)
                {
                    // We are changing direction - this is our corner
                    vertical = !vertical;
                    // Calculate passage between start and stop
                    // Calculate passage inside current node

                    // Calculate new window - new direction
                    if (vertical)
                    {
                        start = new Vector3(window.MiddlePoint(), start.y, z);
                        list.Add(start);

                        window.min = current.bounds.xMin;
                        window.max = current.bounds.xMax;
                        start = current.bounds.center;
                    }
                    else
                    {
                        start = new Vector3(start.x, window.MiddlePoint(), z);
                        list.Add(start);

                        window.min = current.bounds.yMin;
                        window.max = current.bounds.yMax;
                        start = current.bounds.center;
                    }
                }
                else
                {
                    if (vertical)
                    {
                        // Check if new window is contained by current
                        if (window.Contains(current.bounds.yMin, current.bounds.yMax))
                        {
                            window.Shrink(current.bounds.yMin, current.bounds.yMax);
                            //stop = current.bounds.center;
                        }
                        else
                        {
                            list.Add(new Vector3(window.MiddlePoint(), start.y, z));
                            list.Add(new Vector3(window.MiddlePoint(), current.bounds.center.y, z));
                            start = current.bounds.center;

                            // Calculate passage between start and stop
                            // Calculate passage inside current node
                        }
                    }
                    else
                    {
                        if (window.Contains(current.bounds.xMin, current.bounds.xMin))
                        {
                            window.Shrink(current.bounds.xMin, current.bounds.xMin);
                            //stop = current.bounds.center;
                        }
                        else
                        {
                            list.Add(new Vector3(start.x, window.MiddlePoint(), z));
                            list.Add(new Vector3(previous.bounds.center.x, window.MiddlePoint(), z));

                            window.min = current.bounds.yMin;
                            window.max = current.bounds.yMax;
                            start = current.bounds.center;
                        }
                    }
                }

                previous = current;
                current = (QuadNode)current.parent;
            }
        }

        private class Window
        {
            public float min;
            public float max;

            public Window(float min, float max)
            {
                this.min = min;
                this.max = max;
            }

            public bool Contains(Window other)
            {
                return (min <= other.min && max >= other.max) || (other.min <= min && other.max >= max);
            }

            public bool Contains(float min, float max)
            {
                return (this.min <= min && this.max >= max) || (min <= this.min && max >= this.max);
            }

            public float MiddlePoint()
            {
                return (max - min) / 2f;
            }

            public void Shrink(float min, float max)
            {
                this.min = Mathf.Max(this.min, min);
                this.max = Mathf.Min(this.max, max); ;
            }

            public Window Merge(Window other)
            {
                return new Window(Mathf.Max(min, other.min), Mathf.Min(max, other.max));
            }
        }

        //public class TunnelPath
        //{
        //    List<Vector2> path = new List<Vector2>();
        //    Tunnel tunnel;

        //    public void Smooth(Vector2 start, Vector2 end, QuadNode endNode)
        //    {
        //        if (endNode == null) return;

        //        if (endNode.parent == null)
        //        {
        //            // One node only;
        //            Move(start, endNode, end);
        //            return;
        //        }

        //        // Store loop nodes
        //        QuadNode previous = endNode;
        //        QuadNode current = (QuadNode)endNode.parent;

        //        // Move inside start node
        //        Move(start, previous, current);

        //        // Update loop nodes
        //        previous = current;
        //        current = (QuadNode)current.parent;

        //        while (current != null && current != endNode)
        //        {
        //            // We have 2 options in this loop:
        //            //
        //            // 1. Resolve path inside single quad node - because of path starting/ending, turning on corner/moving diagonal inside node <- this ends tunnel
        //            //      - How to recognize if this node requires local treatment? First and last node can always be treated this way -> this goes out of loop.
        //            //
        //            // 2. Start / Continue tunnel
        //            //      - We need to if this can be done

        //            Move(previous, current);


        //            // Move to next node
        //            previous = current;
        //            current = (QuadNode)current.parent;
        //        }

        //        // Resolve position in endNode - current here should be equal endNode
        //        Move(previous, current, end);
        //    }

        //    public void Move(QuadNode from, QuadNode to)
        //    {
        //        // If cannot move with tunnel, break it, store nodes, create new Tunnel
        //    }

        //    public void Move(Vector2 from, QuadNode inside, QuadNode to)
        //    {
        //        path.Insert(0, from);

        //        tunnel = new Tunnel(inside.bounds);
        //    }

        //    public void Move(QuadNode from, QuadNode inside, Vector2 to)
        //    {

        //    }

        //    public void Move(Vector2 from, QuadNode inside, Vector2 to)
        //    {
        //        tunnel.End();
        //    }

        //    public enum Direction
        //    {
        //        top,
        //        right,
        //        bottom,
        //        left
        //    }

        //    public class Tunnel
        //    {
        //        public Vector2 start;
        //        public Rect bounds;
        //        public Direction direction;

        //        public Tunnel(Rect rect)
        //        {
        //            bounds = rect;
        //            start = rect.center;
        //        }

        //        bool isVertical
        //        {
        //            get { return bounds.width < bounds.height; }
        //        }

        //        public void ExpandHorizontal(Rect rect)
        //        {
        //            // Shrink vertical window
        //            bounds.yMin = Mathf.Max(bounds.yMin, rect.yMin);
        //            bounds.yMax = Mathf.Min(bounds.yMax, rect.yMax);

        //            // Are we expanding to left or to the right?
        //            if (rect.xMax > bounds.xMax)
        //            {
        //                // Right
        //                bounds.xMax = bounds.xMax;
        //            }
        //            else
        //            {
        //                // Left
        //                bounds.xMin = bounds.xMin;
        //            }
        //        }

        //        public void ExpandVertically(Rect rect)
        //        {
        //            // Shrink horizontal window
        //            bounds.xMin = Mathf.Max(bounds.xMin, rect.xMin);
        //            bounds.xMax = Mathf.Min(bounds.xMax, rect.xMax);

        //            // Are we expanding to top or to the bottom?
        //            if (rect.yMax > bounds.yMax)
        //            {
        //                // top
        //                bounds.yMax = bounds.yMax;
        //            }
        //            else
        //            {
        //                // bottom
        //                bounds.yMin = bounds.yMin;
        //            }
        //        }

        //        public static Direction GetDirection(Rect from, Rect to)
        //        {
        //            float horizontal = to.center.x - from.center.x;
        //            float vertical = to.center.y - from.center.y;

        //            if (Mathf.Abs(horizontal) > Mathf.Abs(vertical))
        //            {
        //                // We're moving horizontally
        //                return horizontal > 0 ? Direction.right : Direction.left;
        //            }
        //            else
        //            {
        //                // We're moving vertically
        //                return vertical > 0 ? Direction.top : Direction.bottom;
        //            }
        //        }

        //        public void End()
        //        {
        //        }

        //    }
        //}


        void DrawOverlaps(int z)
        {
            if (!drawOverlaps) return;

            if (mouseNode == null) return;

            //mouseNeighbourhood.Clear();
            //mouseNode.GetNeighbours(mouseNeighbourhood);

            //foreach (QuadTree<PathTileComponent>.QuadNode qn in mouseNeighbourhood)
            //{
            //    qn.DebugDraw(Color.blue, z);
            //}

            //mouseNeighbours = mouseNeighbourhood.Count;

            int i = 0;
            foreach (QuadNode qn in mouseNode.GetNeighbours())
            {
                qn.DebugDraw(Color.blue, z);
                ++i;
            }

            mouseNeighbours = i;
        }

        void DrawNeighboursFast(int z)
        {
            if (!drawOverlapsFast) return;
            if (mouseNode == null) return;

            Color purple = new Color(0.66f, 0.125f, 0.94f);
            foreach (QuadNode qn in mouseNode.GetNeighbours())
            {
                if (qn != null)
                    qn.DebugDraw(purple, z);
            }
        }

        void DrawMouseNode(int z)
        {
            Rect testRect = new Rect(mouseNode.bounds);
            testRect.DebugDraw(Color.cyan, z);
        }

        void PlaceTile(Vector3 worldPosition, bool remove)
        {
            Vector3 p = GridToWorldPosition(WorldToGridPosition(worldPosition));

            Ray ray = new Ray(p - Vector3.forward, Vector3.forward);
            RaycastHit hitInfo;

            bool hasHit = Physics.Raycast(ray, out hitInfo);

            if (!hasHit && !remove)
            {
                // Create object
                GameObject go = Instantiate(tilePrefab, p - Vector3.forward * 0.001f, Quaternion.identity) as GameObject;
                go.transform.parent = tileHolder.transform;
                //Tile ptc = go.GetComponent<Tile>();
                ++tileCount;

                // Insert it to quad tree
                quadTree.Insert(go.GetComponent<Tile>(), new Rect(go.transform.position.x - tileSize / 2f, go.transform.position.y - tileSize / 2f, tileSize, tileSize));
            }

            if (hasHit && remove)
            {
                // Destroy game object if flag set
                if (hitInfo.collider.CompareTag("Tile"))
                {
                    Tile ptc = hitInfo.collider.GetComponent<Tile>();
                    quadTree.Remove(quadTree.GetNode(p));
                    --tileCount;
                    Destroy(ptc.gameObject);
                    return;
                }
            }
        }

        Vector2 WorldToGridPosition(Vector3 worldPosition)
        {
            return new Vector2(Mathf.RoundToInt((worldPosition.x - 0.5f) / tileSize), Mathf.RoundToInt((worldPosition.y - 0.5f) / tileSize));
        }

        Vector3 GridToWorldPosition(Vector2 gridPosition)
        {
            return new Vector3((gridPosition.x + 0.5f) * tileSize, (gridPosition.y + 0.5f) * tileSize);
        }

        void OnDebugDraw()
        {
            int z = -1;

            if (debugEnabled)
            {
                quadTree.DebugDraw(Color.red);
            }

            DrawPath(2 * z);

            if (debugEnabled)
            {
                DrawOverlaps(3 * z);
                DrawNeighboursFast(4 * z);
                DrawMouseNode(5 * z);
            }
        }
    }

    public class QuadTreeTraversor : IPathAgent
    {
        static QuadTreeTraversor _instance;
        public static QuadTreeTraversor Instance
        {
            get
            {
                if (_instance == null) _instance = new QuadTreeTraversor();
                return _instance;
            }
        }

        public int CostToGoal(PathNode current, PathNode goal)
        {
            QuadNode c = (QuadNode)current;
            QuadNode g = (QuadNode)goal;

            return (int)((Mathf.Abs(g.bounds.center.x - c.bounds.center.x) + Mathf.Abs(g.bounds.center.y - c.bounds.center.y)) * c.level);
        }

        public int TraverseCost(PathNode start, PathNode current)
        {
            //QuadNode s = (QuadNode)start;
            QuadNode c = (QuadNode)current;

            int r = 9 - c.level;
            return r * r;
        }

        public bool IsBlocked(PathNode from, PathNode to)
        {
            return ((QuadNode)to).holder != null;
        }
    }
}