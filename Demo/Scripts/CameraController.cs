﻿using UnityEngine;

namespace Moonlit.Pathfinding.Demo
{
    public class CameraController : MonoBehaviour
    {
        public float minOrtographicSize = 5f;
        public float maxOrtographicSize = 35f;
        public float zoomSpeed = 2f;

        private Vector3 dragOrigin;
        private Vector2 dragMousePosition;
        private Vector2 deltaMousePosition;

        void Update()
        {
            if ((Input.GetKey(KeyCode.Space) && Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(2)))
            {
                dragOrigin = transform.position;
                dragMousePosition = Input.mousePosition;
            }

            if ((Input.GetKey(KeyCode.Space) && Input.GetMouseButton(0)) || Input.GetMouseButton(2))
            {
                deltaMousePosition = new Vector2(dragMousePosition.x - Input.mousePosition.x, dragMousePosition.y - Input.mousePosition.y);
            }

            float zoom = Input.GetAxis("Mouse ScrollWheel");
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - zoom * zoomSpeed, minOrtographicSize, maxOrtographicSize);
        }

        void LateUpdate()
        {
            if ((Input.GetKey(KeyCode.Space) && Input.GetMouseButton(0)) || Input.GetMouseButton(2))
            {
                transform.position = dragOrigin + Camera.main.ScreenToViewportPoint(deltaMousePosition * camera.orthographicSize * 3);
            }
        }
    }
}