﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Moonlit.Pathfinding.Utils
{
    public class BinaryHeap<T> where T : IComparable<T>
    {
        public class HeapNode
        {
            public int index;
            public T value;

            public int left { get { return 2 * index + 1; } }
            public int right { get { return 2 * index + 2; } }
            public int parent { get { return (index - 1) / 2; } }

            public override string ToString()
            {
                return "Node(i: " + index + " / v: " + value.ToString();
            }
        }

        int count = 0;

        HeapNode[] heap;

        public BinaryHeap(int capacity)
        {
            heap = new HeapNode[capacity];
        }

        public HeapNode Insert(T item)
        {
            HeapNode node = new HeapNode();
            node.value = item;
            node.index = count;

            heap[count] = node;

            count++;

            DecreaseKey(node);

            return node;
        }

        public HeapNode ExtractMinimum()
        {
            switch (count)
            {
                case 0:
                    return null;
                case 1:
                    --count;
                    return heap[0];
                default:
                    HeapNode minimum = heap[0];
                    heap[0] = heap[--count];
                    heap[0].index = 0;
                    IncreaseKey(heap[0]);
                    return minimum;
            }
        }

        //int temp;
        HeapNode temp;
        int parentIndex;
        int currentIndex;
        int leftIndex;
        int rightIndex;
        int minIndex;

        public void DecreaseKey(HeapNode node)
        {
            while (node.index > 0)
            {
                parentIndex = node.parent;
                currentIndex = node.index;

                temp = heap[parentIndex];

                if (node.value.CompareTo(temp.value) > 0) break;

                heap[parentIndex] = node;
                heap[currentIndex] = temp;

                temp.index = currentIndex;
                node.index = parentIndex;
            }
        }

        public void IncreaseKey(HeapNode node)
        {
            while (node.index < count)
            {
                leftIndex = node.left;
                rightIndex = node.right;

                if (leftIndex >= count)
                {
                    if (rightIndex >= count)
                    {
                        return;
                    }
                    else
                    {
                        temp = heap[rightIndex];
                    }
                }
                else
                {
                    if (rightIndex >= count)
                    {
                        temp = heap[leftIndex];
                    }
                    else
                    {
                        if (heap[leftIndex].value.CompareTo(heap[rightIndex].value) <= 0)
                        {
                            temp = heap[leftIndex];
                        }
                        else
                        {
                            temp = heap[rightIndex];
                        }
                    }
                }

                if (temp.value.CompareTo(node.value) < 0)
                {
                    currentIndex = node.index;
                    minIndex = temp.index;

                    heap[currentIndex] = temp;
                    heap[minIndex] = node;

                    temp.index = currentIndex;
                    node.index = minIndex;
                }
                else
                {
                    return;
                }
            }
        }

        public int Capacity { get { return heap.Length; } }

        public int Count { get { return count; } }

        public bool HasRoom()
        {
            return count < heap.Length;
        }

        public void Clear()
        {
            heap = new HeapNode[heap.Length];
            count = 0;
        }

        public override string ToString()
        {
            string s = "";
            int depth = 0;
            for (int i = 0; i < count; ++i)
            {

                int d = Mathf.FloorToInt(Mathf.Log(i + 1) / Mathf.Log(2));
                if (depth < d)
                {
                    depth = d;
                    s += "\n\n";
                }

                s += heap[i].value + "\t";
                //s += "d:" + d + " | \t" + heap[i].value + "\n";


            }
            return s + "\n\nCount: " + count;
        }
    }

    public class HOTQueue : PathNodeQueue
    {
        BinaryHeap<PathNode> heap;
        Queue<PathNode> queue;

        public HOTQueue(int heapCapacity)
        {
            heap = new BinaryHeap<PathNode>(heapCapacity);
            queue = new Queue<PathNode>();
        }

        public void Enqueue(PathNode node)
        {
            if (heap.HasRoom())
            {
                heap.Insert(node);
            }
            else
            {
                queue.Enqueue(node);
            }
        }

        public PathNode Dequeue()
        {
            PathNode node = heap.ExtractMinimum().value;
            if (queue.Count > 0) heap.Insert(queue.Dequeue());
            return node;
        }

        public void Update(PathNode node)
        {
            //throw new NotImplementedException();
        }

        public void Clear()
        {
            heap.Clear();
            queue.Clear();
        }

        public int Count
        {
            get { return heap.Count + queue.Count; }
        }
    }

    public static class ExtensionMethods
    {
        public static void DebugDraw(this Rect rect, Color color, int z = 0)
        {
            // Top
            Debug.DrawLine(new Vector3(rect.xMin, rect.yMax, z), new Vector3(rect.xMax, rect.yMax, z), color);

            // Right
            Debug.DrawLine(new Vector3(rect.xMax, rect.yMax, z), new Vector3(rect.xMax, rect.yMin, z), color);

            // Bottom
            Debug.DrawLine(new Vector3(rect.xMax, rect.yMin, z), new Vector3(rect.xMin, rect.yMin, z), color);

            // Left
            Debug.DrawLine(new Vector3(rect.xMin, rect.yMin, z), new Vector3(rect.xMin, rect.yMax, z), color);
        }

        public static void DebugDrawFill(this Rect rect, Color color, int z = 0)
        {
            rect.DebugDraw(color, z);

            // TopLeft
            Debug.DrawLine(new Vector3(rect.xMin, rect.yMin, z), new Vector3(rect.xMax, rect.yMax, z), color);

            // Right
            Debug.DrawLine(new Vector3(rect.xMin, rect.yMax, z), new Vector3(rect.xMax, rect.yMin, z), color);
        }

        internal static bool OverlapsEquals(this Rect rect, Rect other)
        {
            return
                other.xMax + 10E-6 > rect.xMin
                &&
                other.xMin - 10E-6 < rect.xMax
                &&
                other.yMax + 10E-6 > rect.yMin
                &&
                other.yMin - 10E-6 < rect.yMax;
        }
    }
}